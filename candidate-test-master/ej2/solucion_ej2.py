import re

with open("testcase.v") as archivo:
    contenido= archivo.read()
    for l in archivo:
        contenido+= archivo.readline()

patron= re.compile(r'  reg \[(.*)\] (\S*) \[(.*)\];\n  initial begin\n((    \S*\[\S*\] = \S*;\n)*)  end\n')
lineas= patron.search(contenido)

patron2= re.compile(r'.*h([0-9A-Fa-f]*);\n')

try:
    lineasTruncado= lineas.group(4)
    with open("testcase.v", mode="w+") as archivo:
        lineas= patron.sub(r'  reg ['+lineas.group(1)+r'] \2 ['+ lineas.group(3) + r'];\n  $readmemh("memdump0.mem", mem);\n',contenido)
        archivo.write(lineas)
except AttributeError:
    print("No hay ninguna inicializacón inline de memoria en el archivo de entrada")
else:
    aux=""
    with open("memdump0.mem", mode="w+") as salida:
        salida.write(lineasTruncado)
        salida.seek(0)
        try:
            for l in salida:
                aux+= patron2.search(l).group(1) +"\n"
        except AttributeError:
            print("Error: asegurese de que los valores de memoria sean hexadecimales, y de que la definición de la misma sea correcta ")
        else:
            salida.seek(0)
            salida.truncate()
            salida.write(aux)




