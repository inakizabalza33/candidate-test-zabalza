def test(obtenido, esperado):
    with open(obtenido) as file1:
        with open(esperado) as file2:
            x= file1.readlines()
            y= file2.readlines()
    if(len(x)==len(y)):
        exito= True
        for i,j in zip(x,y):
            if (not (i == j)):
                print("El archivo "+ obtenido + " no es el esperado")
                exito= False
                break
        if(exito):
            print("El archivo "+ obtenido + " creado es correcto")
    else:
        print("El archivo "+ obtenido + " no es el esperado")
            
            
test("testcase.v","./expected/expected.v")
test("memdump1.mem","./expected/memdump0.mem")
